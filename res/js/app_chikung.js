var snd_dir = 'res/snd/';

var yooong_snd = new Howl({
  src: [snd_dir+'yooong.mp3']
});
var part1_snd = new Howl({
  src: [snd_dir+'sharpbell.mp3'],
  volume: 0.5
});
var part2_snd = part1_snd;
var end_chikung_snd = new Howl({
  src: [snd_dir+'end.mp3'],
  volume: 0.5
});
var start_chikung_snd = new Howl({
  src: [snd_dir+'start.mp3'],
  volume: 0.5
});
var tick_snd = new Howl({
  src: [snd_dir+'tick.mp3'],
  volume: 0.1
});

var preparation_sec;
var chikung_length_min;
var chikung_part_length_ms;

var preparation_thread, chikung_thread, ticking_thread;

var ithreads = [];
var tthreads = [];

var clock;

var yooong_chikung;
    
$(document).ready(function() {
 
  // Init nosleep.js
  var noSleep = new NoSleep();
  document.addEventListener('click', function enableNoSleep() {
    document.removeEventListener('click', enableNoSleep, false);
    noSleep.enable();
  }, false);
  
  clock = $('#clock').FlipClock({
    clockFace: 'MinuteCounter',
    autoStart: false,
    countdown: true
  });

  yooong_chikung = {
    killThreads: function(){
      ithreads.forEach(function(element, index, array) {
        clearTimeout(element);
      });
      tthreads.forEach(function(element, index, array) {
        clearInterval(element);
      });
      ithreads = tthreads = [];
    },
    part1: function(chikung_length_min) {
      clearInterval(ticking_thread);
      console.log("part1(chikung_part_length_ms: "+chikung_part_length_ms+")");
      clock.start();      
      part1_snd.play();
      chikung_thread = setTimeout(yooong_chikung.part2, chikung_part_length_ms);
      tthreads.push(chikung_thread);
    },
    part2: function() {
      console.log("part2(chikung_part_length_ms: "+chikung_part_length_ms+")");
      part2_snd.play();
      chikung_thread = setTimeout(yooong_chikung.stop, chikung_part_length_ms);
      tthreads.push(chikung_thread);
    },
    tick: function() {
      tick_snd.play();
    },
    reset: function () {
      clock.stop();
      clock.reset();
      noSleep.disable();
      $('#startChikungBtn').attr("disabled",false);
      $('#stopChikungBtn').attr("disabled",true);
    },
    start: function() {
      preparation_sec = $('#preparationInput').val();
      chikung_length_min = $('#chikungLengthInput').val();
      chikung_part_length_ms = (chikung_length_min/2) * 60 * 1000;
      console.log("start( chikung_length_min: "+chikung_length_min+", preparation_sec: "+preparation_sec+") -> ms: " + chikung_part_length_ms);
      preparation_ms = preparation_sec * 1000;
      start_chikung_snd.play();
      noSleep.enable();

      chikung_length_sec = chikung_length_min*60;
      clock.setTime(chikung_length_sec);

      ticking_thread = setInterval(yooong_chikung.tick ,1000);
      ithreads.push(ticking_thread);
      preparation_thread = setTimeout(yooong_chikung.part1, preparation_ms);
      tthreads.push(preparation_thread);

      $('#startChikungBtn').attr("disabled",true);
      $('#stopChikungBtn').attr("disabled",false);
    },
    stop: function() {
      console.log("stop()");
      yooong_chikung.killThreads();
      yooong_chikung.reset();      
      end_chikung_snd.play();
    }
  }

  var chikung_length = {
    setMinutesValue: function() {
      var chikung_length_min = $('#chikungLengthInput').val();
      var chikung_part_length_min = chikung_length_min / 2; 
      var label = "Chi Kung 2 x "+chikung_part_length_min+" min";
      $("#chikungLengthLabel").text(label);
    }
  }

  $( "#chikungLengthInput" ).on('input', function(){
    chikung_length.setMinutesValue();
  });

  $( "#chikungLengthInput" ).change(function(){
    chikung_length.setMinutesValue();
  });

  $( "#startChikungBtn" ).click(function() {
    yooong_chikung.start();
  });

  $( "#stopChikungBtn" ).click(function() {
    yooong_chikung.stop();
  });

  $( "#logo" ).click(function() {
    yooong_snd.play();
  });

  $('#startChikungBtn').attr("disabled",false);
  $('#stopChikungBtn').attr("disabled",true);

  chikung_length.setMinutesValue();



});


