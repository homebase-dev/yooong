$(document).ready(function() {

  // Init nosleep.js
  var noSleep = new NoSleep();
  document.addEventListener('click', function enableNoSleep() {
    document.removeEventListener('click', enableNoSleep, false);
    noSleep.enable();
  }, false);

  var snd_dir = 'res/snd/';

  var yooong_snd = new Howl({ src: [snd_dir+'yooong.mp3'] });
  // var start_snd = new Howl({ src: [snd_dir+'start.mp3'], volume: 0.5 });
  var stop_snd = new Howl({ src: [snd_dir+'end.mp3'], volume: 0.5 });

  // yooong_snd.once('load', function(){
  //   console.log("yooong_snd len:"+yooong_snd.duration());
  //   yooong_snd.play();
  // });

  qigong = {
    threads: [],
    pointer: 0,
    set_pointer:function(p) {
      console.log("SET_POINTER CALLED: ",p);
      this.pointer = p;
    },
    flatten_playbook: function(pb) {
      var flat = [];
      var tick = {sndfile: '../tick.mp3', desc: 'Tick', duration: 5, is_preparation: true, hidden: true};
      pb.forEach(function(v,i,a) {
        flat.push(v);
        if(v.appendix) {
          flat = flat.concat(v.appendix);
        }
        if(v.exercise) {
          flat.push(tick);
        }
      },this);
      return flat;
    },

    get_long_qigong_playbook: function(exercise_duration) {
      console.log("exercise_duration long: ",exercise_duration);
      var half_duration = Math.ceil(exercise_duration/2);
      var qigong_training = [
        {sndfile: 'get_ready_long.mp3', desc: 'Begin der langen Qi Gong Trainingseinheit. 24 (TODO soundfile falsch) Übungen. Mache dich bereit', duration: qigong.preparation_duration(), is_preparation: true, appendix: [
          {sndfile: 'nb5.mp3', desc: '5', duration: 1, is_preparation: true, hidden:true},
          {sndfile: 'nb4.mp3', desc: '4', duration: 1, is_preparation: true, hidden:true},
          {sndfile: 'nb3.mp3', desc: '3', duration: 1, is_preparation: true, hidden:true},
          {sndfile: 'nb2.mp3', desc: '2', duration: 1, is_preparation: true, hidden:true},
          {sndfile: 'nb1.mp3', desc: '1', duration: 1, is_preparation: true, hidden:true} ]
        },
        {sndfile: 'ex1.mp3', exercise: true, desc: '1. Tai Chi Yin Yang', duration: exercise_duration}, 
        {sndfile: 'ex2.mp3', exercise: true, desc: '2. Die gekreuzten Hände öffnen und schließen', duration: half_duration, appendix: [
          {sndfile: 'change_side.mp3', desc: 'Seite wechseln', duration: half_duration, is_preparation: true, hidden:true} ]
        },
        {sndfile: 'ex3.mp3', exercise: true, desc: '3. Kräuselnde Wellen im Meer', duration: exercise_duration}, 
        //<ZUFÄLLIG>
        {sndfile: 'ex4.mp3', exercise: true, desc: '4. Die Flügel nach links und rechts ausbreiten', duration: exercise_duration}, 
        {sndfile: 'ex5.mp3', exercise: true, desc: '5. Das Wasserrad dreht sich', duration: exercise_duration}, 
        {sndfile: 'ex6.mp3', exercise: true, desc: '6. Das Nashorn schaut in den Mond', duration: exercise_duration}, 
        {sndfile: 'ex7.mp3', exercise: true, desc: '7. Der goldene Affe bietet Früchte an', duration: exercise_duration}, 
        {sndfile: 'ex8.mp3', exercise: true, desc: '8. Die heilige Schildkröte paddelt', duration: exercise_duration}, 
        {sndfile: 'ex9.mp3', exercise: true, desc: '9. Drehung in der Form des Pa Kua', duration: exercise_duration}, 
        {sndfile: 'ex10.mp3', exercise: true, desc: '10. Drachenkopf und Phoenixschwanz', duration: exercise_duration}, 
        {sndfile: 'ex11.mp3', exercise: true, desc: '11. Der Himmelskönig stützt seinen Rücken', duration: exercise_duration}, 
        {sndfile: 'ex12.mp3', exercise: true, desc: '12. Der himmlische König Li hebt eine Pagode', duration: exercise_duration}, 
        {sndfile: 'ex13.mp3', exercise: true, desc: '13. Der fliegende Adler dreht seinen Kopf', duration: exercise_duration}, 
        {sndfile: 'ex14.mp3', exercise: true, desc: '14. Große und kleine goldene Sterne', duration: exercise_duration}, 
        {sndfile: 'ex15.mp3', exercise: true, desc: '15. Hände und Füße treffen sich', duration: exercise_duration}, 
        {sndfile: 'ex16.mp3', exercise: true, desc: '16. Verbrauchte Luft ausatmen und frische Luft einatmen', duration: exercise_duration}, 
        {sndfile: 'ex17.mp3', exercise: true, desc: '17. Der heilige Kranich streckt seine Klauen', duration: exercise_duration}, 
        {sndfile: 'ex18.mp3', exercise: true, desc: '18. Schmetterlinge fliegen in Paaren', duration: exercise_duration}, 
        {sndfile: 'ex19.mp3', exercise: true, desc: '19. Das Nashorn trinkt Wasser', duration: half_duration, appendix: [
          {sndfile: 'change_side.mp3', desc: 'Seite wechseln', duration: half_duration, is_preparation: true, hidden:true } ]
        },
        {sndfile: 'ex20.mp3', exercise: true, desc: '20. Gleichermaßen schön', duration: exercise_duration}, 
        {sndfile: 'ex21.mp3', exercise: true, desc: '21. Das Kind betet zu Kwan-Yin', duration: half_duration, appendix: [
          {sndfile: 'change_side.mp3', desc: 'Seite Wechseln', duration: half_duration, is_preparation: true, hidden:true} ]
        }, 
        {sndfile: 'ex22.mp3', exercise: true, desc: '22. Die Peitsche gleichmäßig weiterdrehen', duration: exercise_duration}, 
        {sndfile: 'ex23.mp3', exercise: true, desc: '23. Die goldenen Blüten fallen sanft', duration: exercise_duration}, 
        //</ZUFÄLLIG>
        {sndfile: 'ex24.mp3', exercise: true, desc: '24. Die Augen schließen und den Geist verjüngen', duration: exercise_duration},
        {sndfile: 'finish.mp3', desc: 'Qi Gong Trainingseinheit beendet', duration: 0, is_preparation: true},
      ];
      return qigong_training;
    },

    get_qigong_playbook: function(training_length, exercise_duration) {
      console.log("exercise_duration: ",exercise_duration);
      var playbook = training_length == 12 ? qigong.get_short_qigong_playbook(exercise_duration) : qigong.get_long_qigong_playbook(exercise_duration);
      return qigong.flatten_playbook(playbook);
    },

    get_short_qigong_playbook: function(exercise_duration) {
      console.log("exercise_duration long: ",exercise_duration);
      var short_playbook = [{sndfile: 'get_ready_short.mp3', desc: 'Begin der kurzen Qi Gong Trainingseinheit. 12 Übungen. Mache dich bereit', duration: qigong.preparation_duration(), is_preparation: true, appendix: [
        {sndfile: 'nb5.mp3', desc: '5', duration: 1, is_preparation: true, hidden:true},
        {sndfile: 'nb4.mp3', desc: '4', duration: 1, is_preparation: true, hidden:true},
        {sndfile: 'nb3.mp3', desc: '3', duration: 1, is_preparation: true, hidden:true},
        {sndfile: 'nb2.mp3', desc: '2', duration: 1, is_preparation: true, hidden:true},
        {sndfile: 'nb1.mp3', desc: '1', duration: 1, is_preparation: true, hidden:true}
      ]}];
      var long_playbook = qigong.get_long_qigong_playbook(exercise_duration);

      // var selection = [1,2,3,4,5,6,7,8,29,30]; // exercises which should always be done
      var selection = [1,2,3,4,24,25]; // exercises which should always be done
      while(selection.length <= 12) {
        var dice = Math.floor(Math.random() * (long_playbook.length-1))+1;
        var was_already_thrown = (selection.indexOf(dice) != -1);
        if(!was_already_thrown) {
          selection.push(dice); 
        }
      }
      selection.sort((a, b) => a - b); // exercises should be done according to their order (order ascending)

      console.log("selection: ", selection);
      console.log("long_playbook: ", long_playbook);
      selection.forEach(function(v,i,a) {
        short_playbook.push(long_playbook[v]);
      },this);
      return short_playbook;
    },

    sleep: function(milliseconds) {
     return new Promise(resolve => setTimeout(resolve, milliseconds));
    },

    play_snd: function(sndfile) {
      var timestamp = new Date().toLocaleString();
      var lng = qigong.language()+'/';
      var snd = new Howl({ src: [snd_dir+lng+sndfile], preload: true });
      console.log("* "+timestamp+" play: "+sndfile+", duration: "+snd.duration());
      snd.load();
      snd.play();
    },

    play: /*async*/ function(playbook, pointer) {
      //soundfile, secondsToWait, continueFunction
      pointer = (pointer === undefined) ? 0 : pointer;

      var current = playbook[pointer];
      console.info("play [playbook:", playbook, "pointer:", pointer, "current: ", current,"]");

      if(!current.hidden) {
        qigong.set_pointer(pointer);
      }

      if(!current) {
        // qigong.stop();
        return;
      }

      qigong.play_snd(current.sndfile);
      qigong.set_active_playbook_item('#playbook-entry-'+pointer, current.desc);
      qigong.scroll_to_playbook_item('#playbook-entry-'+pointer);
      thread = setTimeout(function(){qigong.play(playbook, pointer+1)}, current.duration*1000);
      qigong.threads.push(thread);
    },

    scroll_to_playbook_item: function(entryid) {
      // var parent = $(entryid).parent();
      // var top_offset = parent.position().top;
      // parent.parent().animate({
      //   'margin-top': 0 - top_offset
      // })
    },

    get_playbook_duration_secs: function(playbook, pointer) {
      var playbook_duration_secs=0;
      for(var i=(pointer||0); i<playbook.length; i++) {
        playbook_duration_secs+=playbook[i].duration
      }
      return playbook_duration_secs;
    },

    set_clock_timer: function(playbook, pointer) {
      var playbook_duration_secs = qigong.get_playbook_duration_secs(playbook, pointer);
      qigong.clock.setTime(playbook_duration_secs);
    },

    start: function(pointer) {
      console.info("START CALLED", "nb_exercises:", qigong.nb_exercises(), "pointer:", pointer);
      qigong.stop(pointer); //just in case its already running 
      qigong.display_playbook(qigong.playbook);
      qigong.set_clock_timer(qigong.playbook, pointer);
      qigong.clock.start();
      qigong.play(qigong.playbook, pointer);
      noSleep.enable();

      $("#playbook-list").show();
      $("#startBtn").hide();
      $("#pauseBtn").show();
      $(".well").hide();
      // $("#active-item-img").show(); //TODO load image when imgs available
      $('#startBtn').attr('disabled',true);
      $('#stopBtn').attr('disabled',false);
    },

    display_playbook: function(playbook) {
      var list = $('#playbook-list');
      list.empty(); //clear list first
      playbook.forEach(function(v,i,a) {
        if(!v.hidden){
          list.append('<button type="button" class="list-group-item list-group-item-action" id="playbook-entry-'+i+'" onClick="qigong.start('+i+')">'+v.desc+'</button>');
        }
      });
    },

    set_active_playbook_item: function(entryid, item_desc) {
      var el = $(entryid).length;
      if(!el) {
        return;
      }
      // $('li.active').removeClass('active');
      $('button.active').removeClass('active');
      $(entryid).addClass('active');
      // $('#active-playbook-item').text(item_desc);
    },

    stop: function(pointer) {
      console.log("STOP CALLED");
      stop_snd.play();
      qigong.kill_threads();
      qigong.clock.stop();
      qigong.clock.reset();
      qigong.set_pointer(pointer || 0);
      noSleep.disable();
      $("#startBtn").show();
      $("#pauseBtn").hide();
      $(".well").show();
      $("#active-item-img").hide();
      $("#playbook-list").hide();
      $('#startBtn').attr('disabled',false);
      $('#stopBtn').attr('disabled',true);
    },

    pause: function() {
      console.log("PAUSE CALLED: "+qigong.pointer);
      stop_snd.play();
      qigong.kill_threads();
      qigong.clock.stop();
      noSleep.disable();
      $("#startBtn").show();
      $("#pauseBtn").hide();
      $('#startBtn').attr('disabled',false);
      $('#stopBtn').attr('disabled',false);
    },

    kill_threads: function(){
      qigong.threads.forEach(function(element, index, array) { clearTimeout(element); });
      qigong.threads = [];
    },

    language: function() {
      var lang = $('input[name="language"]:checked').val();
      console.log("language: ",lang);
      return lang;
    },

    nb_exercises: function() {
      var nb = parseInt($('input[name="nb_exercises"]:checked').val(), 10) || 12;
      return nb;
    },

    preparation_duration: function() {
      var duration = parseInt($('input[name="preparation_duration_sec"]').val(), 10) || 10;
      return duration;
    },

    exercise_duration: function() {
      var duration = parseInt($('input[id="exersiceDurationInput"]').val(), 10) || 75;
      return duration;
    },

    init: function() {
      console.log("INIT CALLED");
      qigong.playbook = qigong.get_qigong_playbook(qigong.nb_exercises(), qigong.exercise_duration());

      var duration_secs = qigong.get_playbook_duration_secs(qigong.playbook) 
      var duration_display = parseFloat(duration_secs/60).toFixed(0) + ":"+duration_secs%60+" min"; 
      qigong.clock.setTime(duration_secs);
      var label = 'Qi Gong '+duration_display;
      $('#totalLengthLabel').text(label);

      // qigong.nb_exercises() * qigong.exercise_duration();
      // var duration_min = duration_secs / 60; 
      // qigong.clock.setTime(duration_secs);
      // var label = 'Qi Gong '+duration_min+' min';
      // console.log('INIT label: '+label);
      // $('#totalLengthLabel').text(label);
      $('#startBtn').attr('disabled',false);
      $('#stopBtn').attr('disabled',true);
    },

  }

  qigong.clock = $('#clock').FlipClock({
    clockFace: 'MinuteCounter',
    autoStart: false,
    countdown: true
  });

  qigong.init()

  // Trigger

  $('#exersiceDurationInput').on('input', qigong.init);
  $('#exersiceDurationInput').change(qigong.init);
  $('input[type=radio][name=nb_exercises]').change(qigong.init);
  $('input[type=radio][name=language]').change(qigong.init);

  $('#startBtn').click(function() {
    console.log("startBtn.click("+qigong.pointer+")");
    qigong.start(qigong.pointer);
  });

  $('#stopBtn').click(function() {
    qigong.stop(0);
  });

  $('#pauseBtn').click(function() {
    qigong.pause();
  });

  $('#logo').click(function() {
    yooong_snd.play();
  });


});

