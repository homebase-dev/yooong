var snd_dir = 'res/snd/';

var yooong_snd = new Howl({
  src: [snd_dir+'yooong.mp3']
});

var yin_snd = new Howl({
  src: [snd_dir+'yin_female.mp3']
});
var yang_snd = new Howl({
  src: [snd_dir+'yang_female.mp3']
});
var end_form_snd = new Howl({
  src: [snd_dir+'end.mp3'],
  volume: 0.5
});
var start_form_snd = new Howl({
  src: [snd_dir+'start.mp3'],
  volume: 0.5
});
var tick_snd = new Howl({
  src: [snd_dir+'tick.mp3'],
  volume: 0.1
});

var breath_length_ms;
var action = "yin";
var breath_count = 1;
var max_breaths;
var is_less_perfect;
var tick_count = 1;

var preparation_thread, form_thread, ticking_thread;

var ithreads = [];
var tthreads = [];

var clock;
var yooong;
    
$(document).ready(function() {
  
  // Init nosleep.js
  var noSleep = new NoSleep();
  document.addEventListener('click', function enableNoSleep() {
    document.removeEventListener('click', enableNoSleep, false);
    noSleep.enable();
  }, false);

  clock = $('#clock').FlipClock({
    clockFace: 'MinuteCounter',
    autoStart: false
  });

  yooong = {
    killThreads: function(){
      ithreads.forEach(function(element, index, array) {
        clearTimeout(element);
      });
      tthreads.forEach(function(element, index, array) {
        clearInterval(element);
      });
      ithreads = tthreads = [];
    },
    formStep: function() {
      if(breath_count > max_breaths) {
        yooong.stopForm();
        return;
      }
      console.log("formStep("+action+", ("+breath_count+"/"+max_breaths+"), "+breath_length_ms+" ms)");
      if(action == 'yin') {
        yin_snd.play();
      } else {
        yang_snd.play();
      }
      action = (action === "yin") ? "yang" : "yin";
      breath_count++;      
    },    
    tickPreparation: function() {
      tick_snd.play();   
    },
    tick: function() {
      var is_4th_tick = (tick_count % 4 != 0)
      if(is_4th_tick) {
        tick_snd.play();       
      }
      tick_count++;
    },
    resetForm: function () {
      action = "yin";
      breath_count = 1;
      tick_count=1;
      noSleep.disable();
      $('#startFormBtn').attr("disabled",false);
      $('#stopFormBtn').attr("disabled",true);
    },
    playForm: function() {
      clock.reset();
      clock.start();
      noSleep.enable();

      form_thread = setInterval(yooong.formStep ,breath_length_ms);
      ithreads.push(form_thread);

      clearInterval(ticking_thread);

      yooong.formStep();

      ticking_thread = setInterval(yooong.tick ,breath_length_ms/4);
      ithreads.push(ticking_thread);
    },
    startForm: function() {
      //yooong.stopForm();
      breath_length_ms = $('#breathLengthInput').val();
      max_breaths = $('#breathsInput').val();
      preparation_sec = $('#preparationInput').val();
      var preparation_ms = (preparation_sec*1000);
      is_less_perfect = $('#lessPerfectInput:checked').val();
      console.log("startForm( breath_length_ms: "+breath_length_ms+", max_breaths: "+max_breaths+", preparation_sec: "+preparation_sec+")");
      start_form_snd.play();

      ticking_thread = setInterval(yooong.tickPreparation, 1000);
      ithreads.push(ticking_thread);

      preparation_thread = setTimeout(yooong.playForm, preparation_ms);
      tthreads.push(preparation_thread);

      $('#startFormBtn').attr("disabled",true);
      $('#stopFormBtn').attr("disabled",false);
    },
    stopForm: function() {
      console.log("stopForm()");
      yooong.killThreads();
      clock.stop();
      end_form_snd.play();
      yooong.resetForm();
    }
  }

  var toggle_buttons = {
    toggleButton: function(btn){
      $(btn).toggleClass( "label-default" );
      $(btn).toggleClass( "label-info" );
      toggle_buttons.setBreathValue();
    },
    setBreathValue: function() {
      var is_part1_pressed = $('#part1ToggleBtn').hasClass("label-info");
      var is_part2_pressed = $('#part2ToggleBtn').hasClass("label-info");
      var is_part3_pressed = $('#part3ToggleBtn').hasClass("label-info");
      var breath_number = 0;
      if(is_part1_pressed) {
        breath_number += 46;
      } 
      if(is_part2_pressed) {
        breath_number += 118;
      }
      if(is_part3_pressed) {
        breath_number += 159;
      }
      $("#breathsInput").val(breath_number).change();
    }    
  }

  var form_length = {
    setMinutesValue: function() {
      breath_length_ms = $('#breathLengthInput').val();
      max_breaths = $('#breathsInput').val();
      var form_length_min = (((breath_length_ms/1000) * max_breaths) / 60).toPrecision(2)
      var label = "Form "+form_length_min+" min";
      $("#formLengthLabel").text(label);
    }
  }

  $( "#part1ToggleBtn, #part2ToggleBtn, #part3ToggleBtn" ).click(function() {
    toggle_buttons.toggleButton(this);
  });

  $( "#breathLengthInput, #breathsInput" ).on('input', function(){
    form_length.setMinutesValue();
  });

  $( "#breathLengthInput, #breathsInput" ).change(function(){
    form_length.setMinutesValue();
  });

  $( "#startFormBtn" ).click(function() {
    yooong.startForm();
  });

  $( "#stopFormBtn" ).click(function() {
    yooong.stopForm();
  });

  $( "#logo" ).click(function() {
    yooong_snd.play();
  });

  $('#startFormBtn').attr("disabled",false);
  $('#stopFormBtn').attr("disabled",true);

  toggle_buttons.setBreathValue();
  form_length.setMinutesValue();


});


