# Yooong

Yooong is a simple web app specifically designed to make your ITCCA training a bit easier:

[Yooong gitlab page](https://homebase-dev.gitlab.io/yooong/)

It currently supports following training methods:
* **qi gong**: guided qi gong training session
* **chi kung**: a timer for your (yin- and yang-) chi kung training session
* **yin yang form**: provides a yin/yang frame for the form (resp. it speaks the yin/yang form for you, so you can concentrate on performing the form)

## used libraries
* [jQuery mobile](https://jquerymobile.com/)
* [Bootstrap](https://getbootstrap.com/)
* [Bootstrap darkly](https://bootswatch.com/darkly/)
* [howlerjs](https://howlerjs.com/)
* [flipclockjs](https://github.com/objectivehtml/FlipClock)

